const fs             = require('fs')
const ethers         = require('ethers')
const keccak256      = require('keccak256')
const { MerkleTree } = require('merkletreejs')
const abiCoder       = new ethers.utils.AbiCoder

// Holding data
const leaves         = {}
const airdrop        = []

// Hash function (account + data)
function hashToken(account, value) {
	return Buffer.from(ethers.utils.solidityKeccak256(['uint256', 'address'], [value, account]).slice(2), 'hex')
}

// Process raw data
await fs.readFile('PKTTN-AIRDROP-TESTNET.tsv', 'utf8', function(err, data) {

    if(err) {
        console.error(err);
    }

    // Process the raw data    
    data.split('\n').forEach( line => {
        let row = line.split('\t')
        if(row[0]) {            
            airdrop.push({
                addr:  row[0] || '0',
                amt:   row[1] || '0',
                bonus: row[2] || '0',
                sac:   row[3] || '0',
            })
        }
    })

    // Encode values
    airdrop.forEach((item) => {
        let val = ethers.BigNumber.from(item.bonus).shl(64).add(item.sac).shl(64).add(item.amt)
        leaves[item.addr] = abiCoder.encode(['uint256'], [val])
    })

    let merkleTree = new MerkleTree(Object.entries(leaves).map(token => hashToken(...token)), keccak256, { sortPairs: true })

    console.log('ROOT', merkleTree.getHexRoot())
});




