
PulseKittens.io MAINNET AIRDROP
===============================
Source derived from the airdrop tab.

https://docs.google.com/spreadsheets/d/1ma6MYXabG8fLIARXUoR-4fEjmSWVRHsY0ByMuuMzRdA/view

Merkle root verification:

1. npm install
2. rpm run test

Root should match value in contract: 0x64547f9933b53bdcff0ebc3a6e77f58c426209d80fd73547349556ae22410757
